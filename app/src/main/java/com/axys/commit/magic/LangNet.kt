package com.axys.commit.magic

import android.content.Context
import android.util.Log
import org.tensorflow.contrib.android.TensorFlowInferenceInterface
import java.io.InputStream


class LangNet(ctx : Context? = null, inputStream: InputStream? = null) : Classifier{
    companion object {
        val chars = arrayOf('\t', '\n', ' ', '!', '"', '#', '$', '%', '&', "'", '(', ')',
                '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E',
                'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a',
                'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
                'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~')
        private val modelPath = "lang_net.pb"
        private val langs = listOf<String>("Assembly", "C/C++", "Clojure", "C/C++",
             "C#", "CSS", "XML", "Java", "JavaScript", "PHP", "Python", "R", "Ruby")
        val numClasses = 13
        private val zeroState = FloatArray(200,{0.0f})
        private var stateNames = arrayListOf(
                "Model/MultiRNNCellZeroState/BasicLSTMCellZeroState/zeros_1",
                "Model/MultiRNNCellZeroState/BasicLSTMCellZeroState/zeros",
                "Model/MultiRNNCellZeroState/BasicLSTMCellZeroState_1/zeros_1",
                "Model/MultiRNNCellZeroState/BasicLSTMCellZeroState_1/zeros")

        var tfInterface : TensorFlowInferenceInterface? = null
    }

    init {
        if (tfInterface==null){
            if (ctx!=null)
                tfInterface = TensorFlowInferenceInterface(ctx.assets, modelPath)
            else if (inputStream!=null)
                tfInterface = TensorFlowInferenceInterface(inputStream)
        }
    }

    override fun recognizeCode(string: String): List<Classifier.Recognition> {

        val char_indices = string.map<Int> { c ->
            val ind = chars.indexOf(c)
            if (ind==-1) chars.size else ind
        }.toIntArray()

        tfInterface!!.feed("Model/input_plch", char_indices, 1L, string.length.toLong())
        stateNames.forEach{
            tfInterface!!.feed(it, zeroState, 1L, 200L)
        }
        tfInterface!!.run(Array<String>(1, {"Model/out_softmax"}))
        val output = FloatArray(numClasses*string.length)

        tfInterface!!.fetch("Model/out_softmax", output)
        val recList = ArrayList<Classifier.Recognition>()
        var sum = 0.0f
        for (i in 0 until numClasses){
            val ind = (string.length - 1)* numClasses + i
            recList.add(Classifier.Recognition(i.toString(), langs[i], output[ind]))
            sum+=output[ind]
        }
        recList.sortBy { it -> -it.confidence!! }
        recList.forEach{Log.d("Recognition",it.toString())}
        return recList
    }


    override fun close() {
        tfInterface!!.close()
    }
}
