package com.axys.commit.magic

import android.graphics.Bitmap

interface Classifier {

    class Recognition(
            val id: String?,
            val title: String?,
            val confidence: Float?
    ){

        override fun toString(): String {
            var resultString = ""
            if (id != null) {
                resultString += "[$id] "
            }

            if (title != null) {
                resultString += title + " "
            }

            if (confidence != null) {
                resultString += "%.1f".format(confidence * 100.0f)
            }
            return resultString.trim { it <= ' ' }
        }
    }

    fun recognizeCode(string: String): List<Recognition>

    fun close()
}