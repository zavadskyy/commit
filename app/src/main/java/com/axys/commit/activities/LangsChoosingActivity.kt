package com.axys.commit.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.notanexample.zavadskyy.commit.R
import android.provider.ContactsContract
import android.view.ViewGroup
import android.view.Gravity
import android.widget.ProgressBar
import android.app.ListActivity
import android.app.LoaderManager
import android.content.CursorLoader
import android.content.Loader
import android.database.Cursor
import android.view.View
import android.widget.ListView
import android.widget.SimpleCursorAdapter


class LangsChoosingActivity : ListActivity(), LoaderManager.LoaderCallbacks<Cursor> {

    // This is the Adapter being used to display the list's data
    lateinit var mAdapter: SimpleCursorAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val root = findViewById<ViewGroup>(android.R.id.content)


        // For the cursor adapter, specify which columns go into which views
        val fromColumns = arrayOf(ContactsContract.Data.DISPLAY_NAME)
        val toViews = intArrayOf(android.R.id.text1) // The TextView in simple_list_item_1

        // Create an empty adapter we will use to display the loaded data.
        // We pass null for the cursor, then update it in onLoadFinished()
        mAdapter = SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1, null,
                fromColumns, toViews, 0)
        listAdapter = mAdapter

        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        loaderManager.initLoader(0, null, this)
    }

    // Called when a new Loader needs to be created
    override fun onCreateLoader(id: Int, args: Bundle): Loader<Cursor> {
        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        return CursorLoader(this, ContactsContract.Data.CONTENT_URI,
                PROJECTION, SELECTION, null, null)
    }

    override fun onLoadFinished(loader: Loader<Cursor>, data: Cursor) {
        mAdapter.swapCursor(data)
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        mAdapter.swapCursor(null)
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {

    }

    companion object {

        // These are the Contacts rows that we will retrieve
        internal val PROJECTION = arrayOf(ContactsContract.Data._ID, ContactsContract.Data.DISPLAY_NAME)

        // This is the select criteria
        internal val SELECTION = "((" +
                ContactsContract.Data.DISPLAY_NAME + " NOTNULL) AND (" +
                ContactsContract.Data.DISPLAY_NAME + " != '' ))"
    }
}
