package com.axys.commit.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import com.axys.commit.knowledge.Profile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings

class SplashActivity : AppCompatActivity() {

    private var account : FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        val settings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build()
        val db = FirebaseFirestore.getInstance()
        db.firestoreSettings = settings

        val mAuth = FirebaseAuth.getInstance()
        account = mAuth.currentUser
    }

    override fun onStart() {
        super.onStart()

        if (account!=null)
            Profile.loggedUser = Profile(account!!, { initialized ->
                if (initialized)
                    startActivity(Intent(this, MainActivity::class.java))
                else
                    startActivity(Intent(this, ProfileActivity::class.java))
                finish()
            })
        else {
            startActivity(Intent(this, SigninActivity::class.java))
            finish()
        }
    }

}

