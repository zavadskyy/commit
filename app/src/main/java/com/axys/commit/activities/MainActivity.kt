package com.axys.commit.activities

import android.content.Context
import android.os.Bundle
import android.os.Vibrator
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Log
import android.util.Xml
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.*
import com.axys.commit.knowledge.Profile
import com.axys.commit.magic.LangNet
import com.axys.commit.misc.NiceEditText
import com.axys.commit.misc.NiceLinearLayout
import com.axys.commit.misc.NiceScrollView
import com.google.firebase.firestore.FirebaseFirestore
import com.notanexample.zavadskyy.commit.R
import io.github.kbiakov.codeview.CodeView
import io.github.kbiakov.codeview.adapters.Options
import io.github.kbiakov.codeview.highlight.ColorTheme
import java.lang.Math.*
import android.graphics.Typeface




class MainActivity : AppCompatActivity(), TextWatcher,
        GestureDetector.OnGestureListener {

    val DEBUG_TAG = "Main Activity Debug: "
    private val Y_THRESHOLD = 60

    var codeView : CodeView? = null
    var editText : NiceEditText? = null
    var langLabel : TextView? = null
    var scroll : NiceScrollView? = null
    var postButton : Button? = null
    var layout : NiceLinearLayout? = null

    var detector: GestureDetector? = null
    var langNet: LangNet? = null

    var previous: Pair<Float, Float>? = null
    var accumulated: FloatArray = FloatArray(2,{.0f})
    var recoil: Boolean = false
    var manual : Boolean = false
    var langChangeActive = false

    var db : FirebaseFirestore? = null


    val langMap = hashMapOf(
            "Assembly" to "asm",
            "C/C++" to "c",
            "Clojure" to "clj",
            "C#" to "cs",
            "CSS" to "css",
            "XML" to "xml",
            "Java" to "java",
            "JavaScript" to "js",
            "PHP" to "php",
            "Python" to "py",
            "R" to "r",
            "Ruby" to "ruby")

    val spinnerArray = langMap.keys.toTypedArray().sortedArray()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        codeView = findViewById<CodeView>(R.id.code_view)
        codeView?.setOptions(Options.Default.get(this)
                .withTheme(ColorTheme.MONOKAI))

        editText = findViewById(R.id.edit_main)
        editText?.addTextChangedListener(this)

        val istr = assets.open( "frozen_lang_net.pb" )
        langNet = LangNet(inputStream = istr)

        langLabel = findViewById<TextView>(R.id.code_lang_label)
        langLabel?.text = spinnerArray[0]

        detector = GestureDetector(this,this)
        detector?.setIsLongpressEnabled(true)

        scroll = findViewById<NiceScrollView>(R.id.main_activity_scroll)
        scroll?.callback = {e -> if (e!=null) this.onTouchEvent(e) else false}

        postButton = findViewById<Button>(R.id.post_button)
        postButton?.setOnClickListener{ post() }

        layout = findViewById<NiceLinearLayout>(R.id.main_activity_layout)

        codeLabelAttrs = getAttrs(R.xml.default_codelabel)
        codeViewAttrs = getAttrs(R.xml.default_codeview)

        val tf = Typeface.createFromAsset(assets, "fonts/Consolas.ttf")
        editText?.setTypeface(tf, Typeface.NORMAL)

        db = FirebaseFirestore.getInstance()
        inflate()
    }

    fun createViews(){
        codeView = CodeView(this, codeViewAttrs!!)
        codeView?.setOptions(Options.Default.get(this)
                .withTheme(ColorTheme.MONOKAI))
        langLabel = TextView(this, codeLabelAttrs!!)
        langLabel?.text = spinnerArray[0]
        codeView?.label = langLabel
        layout?.addView(langLabel)
        layout?.addView(codeView)
        editText?.setText("")
    }

    fun inflate(){
        val email = Profile.loggedUser?.firebaseAccount?.email!!
        val ref = db!!.collection("profiles").document(email).collection("code")
        ref.get().addOnCompleteListener { task ->
            val coll = task.result
            if (task.isSuccessful) {
                val sorted = coll.sortedBy { it.getLong("time") }
                sorted.forEach {
                    createViews()
                    codeView?.dbID = it.id
                    val lang = it.getString("language")
                    changeLanguage(spinnerArray.indexOf(lang))
                    changeCodeviewText(it.getString("code"))
                }
            } else Log.d("FUCK","Something went horribly wrong")
            createViews()
        }
    }

    fun post(){
        val data = hashMapOf<String, Any>(
                "language" to langLabel!!.text,
                "code" to editText!!.text!!.toString(),
                "time" to System.currentTimeMillis()
        )
        val email = Profile.loggedUser?.firebaseAccount?.email!!
        val docRef = db?.collection("profiles")?.document(email)?.collection("code")
        docRef?.add(data)?.addOnSuccessListener {
            Log.d("All good","Uploaded this shit to DB")
        }?.addOnFailureListener{ Log.d("FUCK","Fuck!")}
        codeView?.dbID = docRef?.id

        createViews()
    }

    fun changeCodeviewText(s : String){
        val scale = applicationContext.resources.displayMetrics.density
        val dps = (s.filter{c -> c=='\n'}.count()+1)*19
        val pixels = (dps * scale + 0.5f).toInt()
        codeView?.setOptions(codeView?.getOptions()!!.withCode(s))
        codeView?.layoutParams?.height = pixels
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (before< s!!.length){
            val text = editText?.text.toString()
            if (s.isNotEmpty() && !manual && (count%10 == 0 ||
                    s.substring(s.length - 1) == " ")) {
                val inference = langNet!!.recognizeCode(text)
                changeLanguage(spinnerArray.indexOf(inference[0].title!!))
            }
        }
    }

    override fun afterTextChanged(p0: Editable?){
        if (p0!!.isEmpty()) manual=false
        changeCodeviewText(p0.toString())
    }

    fun changeLanguage(id: Int) {
        codeView?.setOptions(codeView?.getOptions()!!
                .withLanguage(spinnerArray[id])
                .withCode(editText?.text.toString()))
        langLabel?.text = spinnerArray[id]
    }

    override fun onTouchEvent(event: MotionEvent) : Boolean {
        detector?.onTouchEvent(event)
        when (event.action){
            MotionEvent.ACTION_DOWN -> {
                previous = Pair(event.rawX,event.rawY)
                accumulated.fill(0.0f)
                return true
            }
            MotionEvent.ACTION_UP -> {
                accumulated.fill(0.0f)
                recoil = false
                previous=null
                langChangeActive=false
                scroll?.isEnabled = true
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                if (previous!=null){

                    val dx = event.rawX - previous!!.first
                    val dy = event.rawY - previous!!.second

                    accumulated[0]+=dx
                    accumulated[1]+=dy

                    if (abs(accumulated[1])>Y_THRESHOLD && langChangeActive) {
                        val dIdx = (accumulated[1]/Y_THRESHOLD).toInt()

                        val oldIndex = spinnerArray.indexOf(langLabel!!.text)
                        var newIndex = oldIndex-dIdx

                        newIndex = max(0, min(newIndex,spinnerArray.size-1))
                        if (newIndex!=oldIndex) changeLanguage(newIndex)

                        accumulated.fill(0.0f)

                        manual = true
                        previous = Pair(event.rawX,event.rawY)
                        return true
                    }
                }
                return false
            }
            else -> return super.onTouchEvent(event);
        }
    }

    override fun onLongPress(p0: MotionEvent?){
        if (!langChangeActive) {
            val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            v.vibrate(10)
            langChangeActive = true
            scroll?.isEnabled = false
        }
    }

    fun getAttrs(id : Int) : AttributeSet{
        val parser = resources.getXml(id)
        try {
            parser.next()
            parser.nextTag()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return Xml.asAttributeSet(parser)
    }

    companion object {
        var codeViewAttrs :AttributeSet?=null
        var codeLabelAttrs :AttributeSet?=null
    }

    //region garbage
    override fun onShowPress(p0: MotionEvent?) = Unit
    override fun onSingleTapUp(p0: MotionEvent?) = false
    override fun onDown(p0: MotionEvent?) = false
    override fun onFling(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float) = false
    override fun onScroll(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float) = false
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit

    //endregion
}
