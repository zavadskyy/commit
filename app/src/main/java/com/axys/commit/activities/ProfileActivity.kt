package com.axys.commit.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.axys.commit.knowledge.Profile
import com.axys.commit.misc.DownloadImageTask
import com.google.firebase.firestore.FirebaseFirestore
import com.notanexample.zavadskyy.commit.R
import kotlinx.android.synthetic.main.activity_profile.*
import android.widget.TextView
import android.widget.ViewSwitcher

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        title = "Edit your profile"

        val profile = Profile.loggedUser!!
        DownloadImageTask(findViewById<ImageView>(R.id.profile_image))
                .execute(profile.picRef)
        val nameView = findViewById<TextView>(R.id.name_display)
        nameView.text = profile.name
        name_edit_text.setText(profile.name)
        val continue_button = findViewById<Button>(R.id.continue_button)
        val bio_text = findViewById<EditText>(R.id.bio_text)
        val goals_text = findViewById<EditText>(R.id.goals_text)
        val name_edit_text = findViewById<EditText>(R.id.name_edit_text)
        val db = FirebaseFirestore.getInstance()
        continue_button.setOnClickListener{
            val userData = hashMapOf<String,Any>(
                    "pic_ref" to profile.picRef,
                    "bio" to bio_text.text.toString(),
                    "goals" to goals_text.text.toString(),
                    "name" to name_edit_text.text.toString()
            )
            db.collection("profiles").document(Profile.loggedUser!!.firebaseAccount.email!!)
                    .set(userData)
                    .addOnSuccessListener {
                        Toast.makeText(this,"Profile saved",Toast.LENGTH_SHORT).show()
                    }.addOnFailureListener {
                        e -> Log.w("Adding profile to DB", "failed with", e)
                    }
            startActivity(Intent(this,MainActivity::class.java))
        }
        nameView.setOnClickListener{
            val switcher = findViewById<ViewSwitcher>(R.id.my_switcher)
            switcher.showNext()
        }
    }
}
