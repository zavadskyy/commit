package com.axys.commit.knowledge

import android.util.Log
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*


/**
 * Created by Vladislav Zavadskyy on 06/12/2017.
 */
class Profile(account: FirebaseUser, callback: (Boolean) -> Unit = {}){
    var firebaseAccount : FirebaseUser = account
    lateinit var name : String
    var knows: LinkedList<Item> = LinkedList<Item>()
    var learns: LinkedList<Item> = LinkedList<Item>()
    lateinit var bio : String
    lateinit var goals : String
    lateinit var picRef: String
    var initialized = false

    init {
        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection("profiles").document(account.email!!)
        docRef.get().addOnCompleteListener{ task ->
            if (task.isSuccessful) {
                val document = task.result
                if (document != null && document.exists()) {
                    name = document.getString("name")
                    picRef = document.getString("pic_ref")
                    bio = document.getString("bio")
                    goals = document.getString("goals")

                    //val knowlege = document.data["knows"] as Array<String>
                    //val wannaBeKnowlege = document.data["learns"] as Array<String>
                    //                for (itemID in knowlege) {
                    //                    if (itemID.startsWith("lang"))
                    //                        knows.add(Language(itemID))
                    //                }
                    //                for (itemID in wannaBeKnowlege) {
                    //                    if (itemID.startsWith("lang"))
                    //                        learns.add(Language(itemID))
                    //                }
                    initialized = true
                } else {
                    name = firebaseAccount.displayName ?: ""
                    picRef = firebaseAccount.photoUrl.toString()
                }
                callback(initialized)
            } else {
                Log.d("Get user from DB", "failed with ", task.getException())
            }
        }
    }

    companion object LoggedUser{
        var loggedUser : Profile? = null
    }

    fun learn(subject: Item) = knows.add(subject)
    fun forget(subject: Item) = knows.remove(subject)

    fun wish(subject: Item) = learns.add(subject)
    fun unwish(subject: Item) = learns.remove(subject)

    private val UPDATE_TAG = "User record update: "

    fun updateRecord(){
        val docData = hashMapOf<String, Any>(
                "name" to name,
                "pic_ref" to picRef,
                "knows" to knows,
                "learns" to learns
        )
        val db = FirebaseFirestore.getInstance()
        db.collection("profiles").document(firebaseAccount.email!!)
                .set(docData)
                .addOnSuccessListener({ Log.d(UPDATE_TAG,
                        "DocumentSnapshot successfully written!") })
                .addOnFailureListener({ e -> Log.w(UPDATE_TAG,
                        "Error writing document", e) })
    }
}