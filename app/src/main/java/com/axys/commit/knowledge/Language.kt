package com.axys.commit.knowledge

import android.content.Intent
import android.media.Image
import android.util.Log
import com.axys.commit.activities.MainActivity
import com.axys.commit.activities.ProfileActivity
import com.axys.commit.knowledge.Item
import com.google.firebase.firestore.FirebaseFirestore

/**
 * Created by Vladislav Zavadskyy on 08/12/2017.
 */
class Language(override val id: String): Item(){
    override var name : String? = null
    override var iconRef : String? = null
    init{
        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection("items").document(id)
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result
                if (document != null) {
                    name = document.getString("name")
                    iconRef = document.getString("icon_ref")
                } else Log.d("Item loading: ", "language with id $id is not found");
            } else Log.d("Item loading: ", "get failed with ", task.exception);
        }
    }
}