package com.axys.commit.knowledge

import android.media.Image

/**
 * Created by Vladislav Zavadskyy on 08/12/2017.
 */
abstract class Item {
    abstract val id : String
    abstract val name : String?
    abstract val iconRef : String?
}