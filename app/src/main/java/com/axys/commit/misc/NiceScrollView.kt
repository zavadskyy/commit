package com.axys.commit.misc

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.widget.ScrollView
import android.widget.TextView
import io.github.kbiakov.codeview.CodeView

/**
 * Created by Vladislav Zavadskyy on 19/12/2017.
 */
class NiceScrollView(context: Context, attrs :AttributeSet) : ScrollView(context, attrs) {
    var callback: (MotionEvent?)->Boolean = {true}

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        callback(ev)
        return super.onTouchEvent(ev)
    }
}