package com.axys.commit.misc

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.EditText
import com.axys.commit.activities.MainActivity

class NiceEditText(ctx : Context, attrs : AttributeSet) : EditText(ctx, attrs), GestureDetector.OnGestureListener{

    var gestureDetector : GestureDetector? = null
    init {
        gestureDetector = GestureDetector(ctx, this)
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, p2: Float, p3: Float): Boolean {
        if (e1!=null && e2 !=null)
            when (getSlope(e1.x, e1.y, e2.x, e2.y)) {
                2 -> setText(Regex("[\\s\t]*$").replace(text.toString(),""))
                4 -> {
                    var s = text.toString()
                    if (!(!Regex("\n").containsMatchIn(s) &&
                            !Regex("[^\\s\t]").containsMatchIn(s))
                            && !Regex("\n[\\s\t]*$").containsMatchIn(s))
                        s += "\n"
                    s += "    "
                    setText(s)
                }
            }
        return false
    }

    private fun getSlope(x1: Float, y1: Float, x2: Float, y2: Float): Int {
        val angle = Math.toDegrees(Math.atan2((y1 - y2).toDouble(), (x2 - x1).toDouble()))
        if (angle > 45 && angle <= 135) return 1
        if (angle >= 135 && angle < 180 || angle < -135 && angle > -180) return 2
        if (angle < -45 && angle >= -135) return 3
        return if (angle > -45 && angle <= 45) 4 else 0
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent) : Boolean {
        val result = gestureDetector?.onTouchEvent(event)
        if (result!=null && result) return result
        return super.onTouchEvent(event)
    }

    override fun onShowPress(p0: MotionEvent?)=Unit
    override fun onSingleTapUp(p0: MotionEvent?)=false
    override fun onDown(p0: MotionEvent?)=false
    override fun onScroll(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float)=false
    override fun onLongPress(p0: MotionEvent?) = Unit
}