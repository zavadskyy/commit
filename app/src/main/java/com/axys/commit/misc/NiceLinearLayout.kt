package com.axys.commit.misc

import android.content.Context
import android.support.v7.app.AlertDialog
import android.util.AttributeSet
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.axys.commit.activities.MainActivity
import com.axys.commit.knowledge.Profile
import io.github.kbiakov.codeview.CodeView

/**
 * Created by Vladislav Zavadskyy on 19/12/2017.
 */
class NiceLinearLayout(val ctx: Context, attrs: AttributeSet) : LinearLayout(ctx, attrs),
        GestureDetector.OnDoubleTapListener{

    var gDetector : GestureDetector = GestureDetector(ctx,ctx as MainActivity)
    init {
        gDetector.setOnDoubleTapListener(this)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev!=null) {
            val x = Math.round(ev.getX());
            val y = Math.round(ev.getY());
            for (i in 0 until childCount){
                val child = getChildAt(i)
                if (x > child.left && x < child.right &&
                        y> child.top && y < child.bottom) {
                    selected = when (child) {
                        is TextView -> getChildAt(i+1) as CodeView
                        is CodeView -> child
                        else -> return false
                    }
                }
            }
            gDetector.onTouchEvent(ev)
            return true;
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onDoubleTap(p0: MotionEvent?): Boolean {
        val mainActivity = ctx as MainActivity
        if (selected?.label!=mainActivity.langLabel){
            if (selected!=null) {
                AlertDialog.Builder(ctx)
                        .setTitle("Confirm")
                        .setMessage("Sure you want to delete this?")
                        .setPositiveButton(android.R.string.yes) { _, _ -> deleteCurrent() }
                        .setNegativeButton(android.R.string.no, null).show()
                 return true
            }
        } else {
            Toast.makeText(mainActivity,
                    "For your own sake, you can't delete this.",
                    Toast.LENGTH_SHORT).show()
        }
        return false
    }

    fun deleteCurrent(){
        val mainActivity = ctx as MainActivity
        val dbID = selected?.dbID
        if (dbID!=null) {
            val db = mainActivity.db!!
            val email = Profile.loggedUser?.firebaseAccount?.email!!
            val ref = db.collection("profiles")
                    .document(email)
                    .collection("code")
                    .document(dbID)
            ref.delete().addOnCompleteListener{ t->
                if (t.isComplete){
                    if(t.isSuccessful)
                        Log.d("Everything","Is OK")
                    else Log.d("Капитан","все пошло поп пизде")
                }
            }
        }
        removeView(selected?.label)
        removeView(selected)
    }

    companion object {
        var selected: CodeView? = null
    }

    override fun onDoubleTapEvent(p0: MotionEvent?): Boolean = false
    override fun onSingleTapConfirmed(p0: MotionEvent?): Boolean = false
}